package com.estragon.pamplemousselib.core;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;

public class ICSParser {
	String contenu;
	
	public ICSParser(String contenu) {
		this.contenu = contenu;
	}
	
	private Date convertisseur(String ligne)
	{
		String anne = ligne.substring(0,4);
		String mois = ligne.substring(4,6);
		String jour = ligne.substring(6,8);
		String heure = ligne.substring(9,11);
		String minute = ligne.substring(11,13);
		String seconde = ligne.substring(13,15);
		Date d = new Date(Integer.parseInt(anne)-1900,
				Integer.parseInt(mois)-1,
				Integer.parseInt(jour),
				Integer.parseInt(heure),
				Integer.parseInt(minute),
				Integer.parseInt(seconde));
		return d;
	}

	public ArrayList<Evenement> readMyFile() {
		ArrayList<Evenement> res = new ArrayList<Evenement>();

		try { 

			BufferedReader br = new BufferedReader(new StringReader(contenu));

			String ligne = "";
			while ((ligne = br.readLine()) != null) {
				if (ligne.equals("BEGIN:VEVENT")) {
					res.add(new Evenement());
				}
				else if (ligne.startsWith("DTSTART") || ligne.startsWith("DTEND")) {
					int decalage = ligne.length()-15;
					Date d = convertisseur(ligne.substring(decalage));
					if (ligne.startsWith("DTSTART")) {
						res.get(res.size()-1).setDebut(d);
					}
					else {
						res.get(res.size()-1).setFin(d);
					}
				}
				else if (ligne.startsWith("SUMMARY")) {
					res.get(res.size()-1).setNom(ligne.substring(8));
				}
				else if (ligne.startsWith("LOCATION")) {
					res.get(res.size()-1).setSalle(ligne.substring(9));
				}
				else if (ligne.startsWith("UID")) {
					res.get(res.size()-1).setUid(ligne.substring(4));
				}
			} 

		} catch (IOException e) {
			System.out.println("IOException error!");
			e.printStackTrace();
		}

		return res;

	}

}