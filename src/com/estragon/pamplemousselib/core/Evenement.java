package com.estragon.pamplemousselib.core;

import java.util.Date;

import org.json.me.JSONObject;

public class Evenement {
	
	public static final String[] JOURS = new String[] {"Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Dimanche"};
	public static final String[] MOIS = new String[] {"Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre"};
	
	String uid; //identifiant unique

	Date debut;
	Date fin;
	String nom;
	String salle;
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public Date getDebut() {
		return debut;
	}
	public void setDebut(Date debut) {
		this.debut = debut;
	}
	public Date getFin() {
		return fin;
	}
	public void setFin(Date fin) {
		this.fin = fin;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getSalle() {
		return salle;
	}
	public void setSalle(String salle) {
		this.salle = salle;
	}

	public String toString() {
		return nom+"\nSalle "+salle+datesLisibles();
	}

	public String datesLisibles() {
		String res = "";
		
		//res += JOURS[debut.getDay()-1];
		//res += " "+debut.getDate();
		//res += " "+MOIS[debut.getMonth()];
		res += " de "+debut.getHours()+"h";
		if (debut.getMinutes() != 0) {
			res += debut.getMinutes();
		}
		res += " a "+fin.getHours()+"h";
		if (fin.getMinutes() != 0) {
			res += fin.getMinutes();
		}

		return res;
	}
	
	public JSONObject getJSON() {
		JSONObject object = new JSONObject();
		try {
			object.put("uid", uid);
			object.put("debut", debut.getTime());
			object.put("fin", fin.getTime());
			object.put("nom", nom);
			object.put("salle", salle);
		}
		catch (Exception e) {
			
		}
		
		return object;
	}
}
