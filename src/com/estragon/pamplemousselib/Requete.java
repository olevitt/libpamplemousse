package com.estragon.pamplemousselib;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

public abstract class Requete implements IRequete {
	public static final String BASEURL = "http://62.193.225.66/ensai/";
	public abstract String getLoadingMessage();
	public static CookieStore cookieStore = new BasicCookieStore();
	public static String LT = "";
	public static int GET = 1;
	public static int POST = 2;
	private Exception erreur = null;
	protected HttpRequestBase requete = null;
	protected HttpResponse reponse = null;
	protected String resultat = null;
	List<NameValuePair> parametres = new ArrayList<NameValuePair>(); 
	boolean aborted = false;
	public boolean https = false;

	protected void ajouterParametre(String name,String value) {
		parametres.add(new BasicNameValuePair(name, value));
	}

	public Requete(int methode,String url) {
		if (methode == GET) requete = new HttpGet(url);
		else requete = new HttpPost(url);
		this.requete.setHeader("User-Agent","Mozilla/5.0 (Linux; U; Android 2.3.4; fr-fr; HTC Desire Build/GRJ22) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1 PamplemousseViewer");
		if (!LT.equals("")) {
			this.ajouterParametre("lt", LT);
		}
	}

	public Exception getErreur() {
		return erreur;
	}

	public boolean isAborted() {
		return aborted;
	}

	public boolean hasErreur() {
		return erreur != null;
	}


	public void executer() {
		try {
			if (this.requete.getMethod().equals("POST")) ((HttpPost) (this.requete)).setEntity(new UrlEncodedFormEntity(parametres));
			this.go();
		}
		catch (UnsupportedEncodingException e) {

		}
		
	}

	public void go() {
		try {
			HttpClient client;
			if (https) client = HttpUtils.getNewHttpClient();
			else client = new DefaultHttpClient();
			HttpContext localContext = new BasicHttpContext();
			localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
			reponse = client.execute(this.requete,localContext);
			
			if (reponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK) throw new StatusCodeException(reponse.getStatusLine().getStatusCode());
			
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			reponse.getEntity().writeTo(out);
			out.close();
			client.getConnectionManager().shutdown();
			resultat = out.toString();
			onSuccess(resultat);
		} catch (Exception e) {
			System.out.println(reponse.getStatusLine().getStatusCode());
			e.printStackTrace();
			this.erreur = e;
			onErreur(e);
		}
	}

	/**
	 * Lancé dans le thread graphique
	 */
	public void requeteComplete() {
		//La requete est terminée, on ferme le popup de chargement, s'il y en a un
		if (this.hasErreur()) {
			if (this.isAborted()) {
				//La requete a été annulé par l'utilisateur
				this.onAbort();
			}
			else {
				this.onErreur(erreur);
			}
		}
		else {
				this.onSuccess(this.resultat);
		}
	}
	
	public void abort() {
		aborted = true;
		requete.abort();
	}


	
}
