package com.estragon.pamplemousselib;


public interface IRequete {
	public void onSuccess(String data);
	public void onAbort();
	public void onErreur(Exception erreur);
	
}
