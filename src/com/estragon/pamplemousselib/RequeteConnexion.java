package com.estragon.pamplemousselib;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RequeteConnexion extends Requete implements IRequete {

	private IRequete listener;
	
	public RequeteConnexion(IRequete listener) {
		super(Requete.POST,"https://sso-cas.ensai.fr/cas/login?service=http%3A%2F%2F46.105.117.212%2Fensai%2F");
		///cas/login?service=http%3A%2F%2F46.105.117.212%2Fensai%2F
		this.listener = listener;
		https = true;
	}

	
	@Override
	public void onAbort() {
		// TODO Auto-generated method stub
		listener.onAbort();
	}


	@Override
	public void onSuccess(String data) {
		// TODO Auto-generated method stub
		Pattern pattern = Pattern.compile("<input type=\"hidden\" name=\"lt\" value=\"e([0-9]*)s([0-9]*)\" />");
		Matcher matcher = pattern.matcher(data);
		matcher.find();
		String lt = "";
		try {
			lt = "e"+matcher.group(1)+"s"+matcher.group(2);
			Requete.LT = lt;
			listener.onSuccess(data);
		}
		catch (Exception e) {
			listener.onErreur(e);
		}
	}

	@Override
	public String getLoadingMessage() {
		// TODO Auto-generated method stub
		return "Etape 1/3 : création de la session";
	}

	@Override
	public void onErreur(Exception erreur) {
		// TODO Auto-generated method stub
		listener.onErreur(erreur);
	}

}
