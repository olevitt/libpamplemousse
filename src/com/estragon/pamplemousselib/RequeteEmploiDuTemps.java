package com.estragon.pamplemousselib;

import java.util.List;

import org.json.me.JSONArray;
import org.json.me.JSONObject;

import com.estragon.pamplemousselib.core.Evenement;
import com.estragon.pamplemousselib.core.ICSParser;


public class RequeteEmploiDuTemps extends Requete implements IRequete {

	public static String FICHIER = "pamplemousse.ics";
	private IRequete listener;
	String resultat = null;
	
	public RequeteEmploiDuTemps(IRequete listener) {
		super(Requete.POST,"http://leos.fr/ensai/icalendar_etudiant.php");
		this.listener = listener;
		this.https = false;
	}

	@Override
	public void onSuccess(String data) {
		// TODO Auto-generated method stub
		JSONObject retour = new JSONObject();
		JSONArray events = new JSONArray();
		ICSParser ics = new ICSParser(data);
		List<Evenement> evenements = ics.readMyFile();
		for (Evenement event : evenements) {
			events.put(event.getJSON());
		}
		try {
			retour.put("events", events);
			resultat = retour.toString(1);
		}
		catch (Exception e) {
			
		}
		
			/*try {
				FileOutputStream w = this.context.openFileOutput("pamplemousse.ics",Context.MODE_PRIVATE);
				w.write(data.getBytes());
				w.close();
				Toast.makeText(this.context, "Emploi du temps mis à jour !", Toast.LENGTH_SHORT).show();
			} 
			catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				Toast.makeText(this.context, "Erreur : "+e, Toast.LENGTH_SHORT).show();
			}
			catch (IOException e) {
				Toast.makeText(this.context, "Erreur : "+e, Toast.LENGTH_SHORT).show();
			}
			catch (Exception e) {
				Toast.makeText(this.context, "Erreur : "+e, Toast.LENGTH_SHORT).show();
			}*/
			listener.onSuccess(data);
	}

	@Override
	public String getLoadingMessage() {
		// TODO Auto-generated method stub
		return "Etape 3/3 : téléchargement de l'emploi du temps";
	}

	@Override
	public void onAbort() {
		// TODO Auto-generated method stub
		listener.onAbort();
	}

	@Override
	public void onErreur(Exception erreur) {
		// TODO Auto-generated method stub
		listener.onErreur(erreur);
	}
	
	public String getResultat() {
		return resultat;
	}

}