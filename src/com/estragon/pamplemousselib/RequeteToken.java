package com.estragon.pamplemousselib;


public class RequeteToken extends Requete implements IRequete {

	IRequete listener;

	public RequeteToken(IRequete listener,String pseudo, String mdp) {
		super(Requete.POST,"https://sso-cas.ensai.fr/cas/login?service=http://46.105.117.212/ensai/");
		///cas/login?service=http%3A%2F%2F46.105.117.212%2Fensai%2F
		this.listener = listener;
		this.https = true;

		/*this.ajouterParametre("identifiant", identifiant);
		this.ajouterParametre("motdepasse", mdp);
		this.ajouterParametre("modifmdp", "0");*/
		this.ajouterParametre("submit", "LOGIN");
		this.ajouterParametre("_eventId", "submit");
		this.ajouterParametre("username",pseudo);
		this.ajouterParametre("password",mdp);
	}

	@Override
	public void onSuccess(String data) {
		// TODO Auto-generated method stub
		if (Requete.cookieStore.getCookies().size() >= 2) 
		{
			//Token obtenu
			listener.onSuccess(data);
		}
		else {
			listener.onErreur(new Exception());
		}
		
	}

	@Override
	public String getLoadingMessage() {
		// TODO Auto-generated method stub
		return "Etape 2/3 : obtention d'un token";
	}

	@Override
	public void onAbort() {
		// TODO Auto-generated method stub
		listener.onAbort();
	}

	@Override
	public void onErreur(Exception erreur) {
		// TODO Auto-generated method stub
		listener.onErreur(erreur);
	}

}
